#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

apt-get update
apt-get -y install git

git clone https://github.com/klauern/ansible-things.git

cd ansible-things && ./init.sh
