#!/bin/sh

# initialize things by getting a bare-bones server set up with Ansible
# Run as SUDO

apt-get install -y software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update -y
apt-get upgrade -y
apt-get install -y ansible

ansible-galaxy install joshualund.golang
ansible-galaxy install antoiner77.caddy
/usr/bin/env ansible-playbook site.yml

