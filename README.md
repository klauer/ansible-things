# ansible-things
A repo for storing some bootstrapping Ansible scripts.

## self-bootstrap
I've played a bunch with this, but if you start with a naked Ubuntu 15.10 installation, you can get pretty far with this setup by typing the following:

```sh
bash <(wget -q https://raw.githubusercontent.com/klauern/ansible-things/master/root.sh -O-)
```

which should sorta get where you need to be.



To run on a bare-bones server:

```sh
sudo ./init.sh
```

which should install Ansible.  To configure your `localhost` with the stuff here
simply run `ansible-playbook`:

```sh
ansible-playbook site.yml -K
```
